// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.

package fsrepo

import (
	"errors"
	"fmt"
	"io/fs"
	"os"
	"path/filepath"
	"strings"

	"gitlab.com/zygoon/go-ini"
)

var (
	errEmptyStreamName    = errors.New("stream name cannot be empty")
	errReplacedOpenStream = errors.New("open streams cannot be replaced")
)

// streamFile is a named container for publishing archives.
//
// streamFile contains any number of archives as long as all archive refer to the
// same PackageName, PackageVersion and SourceRevision.
//
// ReplacedBy is the name of non-closed stream used as replacement for closed
// streams.
type streamFile struct {
	Path       string // Location of the .stream file
	Name       string `ini:"Name,[Stream],omitempty"`
	IsClosed   bool   `ini:"Closed,omitempty"`
	ReplacedBy string `ini:"ReplacedBy,omitempty"`

	archives []*archiveFile // Loaded from "${Path}.d/*.archive" files.
}

func openStream(path string) (*streamFile, error) {
	stream := streamFile{Path: path}

	if err := stream.loadIni(); err != nil {
		return nil, fmt.Errorf("cannot load stream %q: %w", path, err)
	}

	if err := stream.computeDefaults(); err != nil {
		return nil, fmt.Errorf("cannot compute defaults of stream %q: %w", path, err)
	}

	if err := stream.loadArchives(); err != nil {
		return nil, fmt.Errorf("cannot load archives in stream %q: %w", path, err)
	}

	if err := stream.verify(); err != nil {
		return nil, fmt.Errorf("cannot verify stream %q: %w", path, err)
	}

	return &stream, nil
}

func (stream *streamFile) loadIni() error {
	f, err := os.Open(stream.Path)
	if err != nil {
		return err
	}

	defer func() {
		_ = f.Close()
	}()

	if err := ini.NewDecoder(f).Decode(stream); err != nil {
		return err
	}

	return nil
}

func (stream *streamFile) computeDefaults() error {
	if stream.Name == "" {
		streamName, err := unescapeStreamName(baseWithoutExt(stream.Path))
		if err != nil {
			return err
		}

		stream.Name = streamName
	}

	return nil
}

func (stream *streamFile) loadArchives() error {
	pathDotD := stream.Path + ".d"

	files, err := os.ReadDir(pathDotD)
	if err != nil {
		if errors.Is(err, fs.ErrNotExist) {
			return nil
		}

		return err
	}

	for _, fi := range files {
		if fi.IsDir() || filepath.Ext(fi.Name()) != ".archive" {
			continue
		}

		arPath := filepath.Join(pathDotD, fi.Name())

		ar, err := openArchive(arPath)
		if err != nil {
			return err
		}

		stream.archives = append(stream.archives, ar)
	}

	return nil
}

func (stream *streamFile) verify() error {
	if stream.Name == "" {
		return errEmptyStreamName
	}

	if !stream.IsClosed && stream.ReplacedBy != "" {
		return errReplacedOpenStream
	}

	return stream.verifyArchives()
}

type defaultKey struct {
	Arch    string
	SubArch string
	Machine string
}

func (stream *streamFile) verifyArchives() error {
	if len(stream.archives) == 0 {
		return nil
	}

	refAr := stream.archives[0]
	refSourceAliases := make(map[string]struct{}, len(refAr.SourceAliases))

	for _, ar := range stream.archives[1:] {
		if ar.PackageName != refAr.PackageName {
			return fmt.Errorf("inconsistent PackageName between %q and %q", ar.Path, refAr.Path)
		}

		if ar.PackageVersion != refAr.PackageVersion {
			return fmt.Errorf("inconsistent PackageVersion between %q and %q", ar.Path, refAr.Path)
		}

		if ar.SourceRevision != refAr.SourceRevision {
			return fmt.Errorf("inconsistent SourceRevision between %q and %q", ar.Path, refAr.Path)
		}

		aliasMismatch := len(ar.SourceAliases) != len(refSourceAliases)
		if !aliasMismatch {
			for _, alias := range ar.SourceAliases {
				if _, ok := refSourceAliases[alias]; !ok {
					aliasMismatch = true
					break
				}
			}
		}

		if aliasMismatch {
			return fmt.Errorf("inconsistent SourceAliases between %q and %q", ar.SourceAliases, refAr.SourceAliases)
		}
	}

	defaults := make(map[defaultKey]*archiveFile)

	for _, ar := range stream.archives {
		if ar.CompatibleDefault {
			key := defaultKey{
				Arch:    ar.CompatibleArch,
				SubArch: ar.CompatibleSubArch,
				Machine: ar.CompatibleMachine,
			}

			if otherAr, ok := defaults[key]; ok {
				return fmt.Errorf("ambiguous CompatibleDefault between %q and %q", ar.Path, otherAr.Path)
			}

			defaults[key] = ar
		}
	}

	return nil
}

func unescapeStreamName(name string) (string, error) {
	// TODO: add escape mechanism.
	return strings.Replace(name, "-", "/", -1), nil
}
