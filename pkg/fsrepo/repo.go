// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.

package fsrepo

import (
	"context"
	"errors"
	"fmt"
	"os"
	"path/filepath"
	"strings"

	"gitlab.com/zygoon/go-ini"
	"gitlab.com/zygoon/netota"
)

// Repo is a collection of named packages.
type Repo struct {
	Path string // Root directory of the repository

	pkgs map[string]*packageFile // Loaded from "${Path}/*.package" files.
}

// Open opens a repository by recursively scanning a given directory.
func Open(path string) (*Repo, error) {
	repo := Repo{Path: path}

	if err := repo.loadIni(); err != nil {
		return nil, fmt.Errorf("cannot load repo %q: %w", path, err)
	}

	if err := repo.loadPackages(); err != nil {
		return nil, fmt.Errorf("cannot use repo %q: %w", repo.Path, err)
	}

	return &repo, nil
}

func (repo *Repo) loadIni() error {
	f, err := os.Open(filepath.Join(repo.Path, "netota.repo"))
	if err != nil {
		// The netota.repo is optional for backward compatibility.
		if errors.Is(err, os.ErrNotExist) {
			return nil
		}

		return err
	}

	defer func() {
		_ = f.Close()
	}()

	if err := ini.NewDecoder(f).Decode(repo); err != nil {
		return err
	}

	return nil
}

type query struct {
	pkgName    string
	streamName string
}

func newQuery(opts []netota.QueryOption) *query {
	q := &query{}

	for _, opt := range opts {
		opt.(queryOption)(q)
	}

	return q
}

func (q *query) visitPackages(repo *Repo, v func(pf *packageFile)) {
	if q.pkgName != "" {
		if pkg, ok := repo.pkgs[q.pkgName]; ok {
			v(pkg)
		}

		return
	}

	for _, pkg := range repo.pkgs {
		v(pkg)
	}
}

func (q *query) visitStreams(repo *Repo, v func(pf *packageFile, sf *streamFile)) {
	q.visitPackages(repo, func(pf *packageFile) {
		if q.streamName != "" {
			if sf, ok := pf.streams[q.streamName]; ok {
				v(pf, sf)
			}

			return
		}

		for _, sf := range pf.streams {
			v(pf, sf)
		}

	})
}

func (q *query) visitArchives(repo *Repo, v func(pf *packageFile, sf *streamFile, af *archiveFile)) {
	q.visitStreams(repo, func(pf *packageFile, sf *streamFile) {
		for _, af := range sf.archives {
			v(pf, sf, af)
		}
	})
}

// QueryOption influences how Query family of functions behave.
type queryOption func(*query)

// QueryOption is a dummy method identifying query option types.
func (queryOption) QueryOption() {}

// PackageName causes Query to select only a particular package.
func (*Repo) PackageName(name string) netota.QueryOption {
	return queryOption(func(q *query) {
		q.pkgName = name
	})
}

// StreamName causes Query to select only a particular stream.
func (*Repo) StreamName(name string) netota.QueryOption {
	return queryOption(func(q *query) {
		q.streamName = name
	})
}

// QueryPackages returns a subset of matching packages.
//
// Use PackageName query option to select a single package. Packages do not
// carry stream information, perform a separate QueryStreams query if required.
func (repo *Repo) QueryPackages(ctx context.Context, opts ...netota.QueryOption) (result []*netota.Package) {
	newQuery(opts).visitPackages(repo, func(pf *packageFile) {
		result = append(result, &netota.Package{
			Name: pf.Name,
		})
	})

	return result
}

// QueryStreams returns a subset of matching streams.
//
// Use PackageName and StreamName query option to select a single package or a
// single stream. Streams do not carry archive information, perform a separate
// QueryArchives query if required.
func (repo *Repo) QueryStreams(ctx context.Context, opts ...netota.QueryOption) (result []*netota.Stream) {
	newQuery(opts).visitStreams(repo, func(pf *packageFile, sf *streamFile) {
		stream := &netota.Stream{
			PackageName: pf.Name,
			Name:        sf.Name,
			ReplacedBy:  sf.ReplacedBy,
			IsClosed:    sf.IsClosed,
		}

		if !sf.IsClosed && len(sf.archives) > 0 {
			af := sf.archives[0]
			// TODO(zyga): compute UpdatedOn
			stream.PackageVersion = af.PackageVersion
			stream.SourceRevision = af.SourceRevision
			stream.SourceAliases = af.SourceAliases
		}

		result = append(result, stream)
	})

	return result
}

// QueryArchives returns a subset of matching archives.
//
// Use PackageName and StreamName query option to select a single package or a
// single stream.
func (repo *Repo) QueryArchives(ctx context.Context, opts ...netota.QueryOption) (result []*netota.Archive) {
	newQuery(opts).visitArchives(repo, func(pf *packageFile, sf *streamFile, af *archiveFile) {
		ar := &netota.Archive{
			ArchiveName:   af.ArchiveName,
			ArchiveSize:   af.ArchiveSize,
			ArchiveType:   netota.ArchiveType(af.ArchiveType),
			ArchiveFormat: netota.ArchiveFormat(af.ArchiveFormat),
			ArchiveChecksums: map[netota.ChecksumType]string{
				netota.Sha256: af.ChecksumSha256,
			},

			ContentType:    netota.ContentType(af.ContentType),
			ContentFormat:  netota.ContentFormat(af.ContentFormat),
			ContentVariant: netota.ContentVariant(af.ContentVariant),
			ContentSize:    af.ContentSize,

			CompatibleArch:    netota.Architecture(af.CompatibleArch),
			CompatibleSubArch: af.CompatibleSubArch,
			CompatibleMachine: af.CompatibleMachine,

			DownloadOffers: []netota.DownloadOffer{{
				URL:    af.DownloadURL,
				Metric: af.DownloadMetric,
			}},
		}

		result = append(result, ar)
	})

	return result
}

func (repo *Repo) loadPackages() error {
	files, err := os.ReadDir(repo.Path)
	if err != nil {
		return err
	}

	for _, fi := range files {
		if fi.IsDir() || filepath.Ext(fi.Name()) != ".package" {
			continue
		}

		pkgPath := filepath.Join(repo.Path, fi.Name())

		pkg, err := openPackage(pkgPath)
		if err != nil {
			return err
		}

		if otherPkg, clash := repo.pkgs[pkg.Name]; clash {
			return fmt.Errorf("package %s defined by both %s and %s", pkg.Name, pkgPath, otherPkg.Path)
		}

		if repo.pkgs == nil {
			repo.pkgs = make(map[string]*packageFile)
		}

		repo.pkgs[pkg.Name] = pkg
	}

	return nil
}

func baseWithoutExt(path string) string {
	return strings.TrimSuffix(filepath.Base(path), filepath.Ext(path))
}
