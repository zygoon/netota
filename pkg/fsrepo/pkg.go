// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.

package fsrepo

import (
	"errors"
	"fmt"
	"os"
	"path/filepath"

	"gitlab.com/zygoon/go-ini"
)

// packageFile describes a Package.
//
// Packages contain one or more streams, each publishing a set of archives
// constrained by having the same SourceRevision and Version.
type packageFile struct {
	Path string // Location of the .package file
	Name string `ini:"Name,[Package],omitempty"`

	streams map[string]*streamFile // Loaded from "${Path}.d/*.stream" files.
}

func openPackage(path string) (*packageFile, error) {
	pkg := packageFile{Path: path}

	if err := pkg.loadIni(); err != nil {
		return nil, fmt.Errorf("cannot load package %q: %w", path, err)
	}

	if err := pkg.computeDefaults(); err != nil {
		return nil, fmt.Errorf("cannot compute defaults of package %q: %w", path, err)
	}

	if err := pkg.loadStreams(); err != nil {
		return nil, fmt.Errorf("cannot load streams of package %q: %w", path, err)
	}

	if err := pkg.verify(); err != nil {
		return nil, fmt.Errorf("cannot verify package %q: %w", path, err)
	}

	return &pkg, nil
}

func (pkg *packageFile) loadIni() error {
	f, err := os.Open(pkg.Path)
	if err != nil {
		return err
	}

	defer func() {
		_ = f.Close()
	}()

	return ini.NewDecoder(f).Decode(pkg)
}

func (pkg *packageFile) computeDefaults() error {
	if pkg.Name == "" {
		pkg.Name = baseWithoutExt(pkg.Path)
	}

	return nil
}

func (pkg *packageFile) loadStreams() error {
	pathDotD := pkg.Path + ".d"

	files, err := os.ReadDir(pathDotD)
	if err != nil {
		if errors.Is(err, os.ErrNotExist) {
			return nil
		}

		return err
	}

	for _, fi := range files {
		if fi.IsDir() || filepath.Ext(fi.Name()) != ".stream" {
			continue
		}

		streamPath := filepath.Join(pathDotD, fi.Name())

		stream, err := openStream(streamPath)
		if err != nil {
			return err
		}

		if otherStream, clash := pkg.streams[stream.Name]; clash {
			return fmt.Errorf("stream %s defined in both %q and %q", stream.Name, streamPath, otherStream.Path)
		}

		if pkg.streams == nil {
			pkg.streams = make(map[string]*streamFile)
		}

		pkg.streams[stream.Name] = stream
	}

	return nil
}

func (pkg *packageFile) verify() error {
	if pkg.Name == "" {
		return errEmptyPackageName
	}

	for _, stream := range pkg.streams {
		if stream.ReplacedBy != "" {
			otherStream := pkg.streams[stream.ReplacedBy]
			if otherStream == nil {
				return fmt.Errorf("stream %s is replaced by unknown stream %s", stream.Path, stream.ReplacedBy)
			}

			if otherStream.IsClosed {
				return fmt.Errorf("stream %s is replaced by closed stream %s", stream.Path, otherStream.Name)
			}
		}
	}

	return nil
}
