// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.

// Package fsrepo implements a NetOTA repository backed by the file system.
//
// This implementation can be used by NetOTA to create and operate a network
// file server for SysOTA. It is scalable to several hundred packages, each
// with a modest number of streams and archives. Beyond this scale it would be
// advisable to switch to a database implementation that does not need to hold
// the entire repository meta-data in memory.
//
// The on-disk format of the repository is a set tree structure rooted at a
// specific directory. All the files in the repository using ini-style format.
// The root of the repository may define any number of ".package" files. Each
// ".package" file has a corresponding ".package.d" directory which defines
// streams. Streams are defined through any number of ".stream" files. Each
// ".stream" file has a corresponding ".stream.d" directory which defines
// archives.  Archives are defined through ".archive" files which end the
// structure.
//
// For precise format refer to the documentation of the Package, Stream and
// Archive types. Their structure tags define the mapping to ini sections and
// fields.
//
// Two data structures enforce unique name constraints: the set of packages in
// a repository and the set of streams in a package. All other types enforce
// additional validation constraints on their contents.
//
// Stream names can be escaped. A logical stream with name such as
// "latest/stable" is defined in the file "latest-stable.stream". This allows
// to keep the structure flat and predicable.
package fsrepo
