// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.

package fsrepo_test

import (
	"context"
	"os"
	"path/filepath"
	"sort"
	"testing"

	"gitlab.com/zygoon/netota/pkg/fsrepo"
	"gitlab.com/zygoon/netota/pkg/slices"
)

func TestOpenEmptyBareRepo(t *testing.T) {
	d := t.TempDir()

	r, err := fsrepo.Open(d)
	if err != nil {
		t.Fatalf("cannot open repo: %s", err)
	}

	if r.Path != d {
		t.Fatalf("r.Path mismatch: got %s, expected: %s", r.Path, d)
	}
}

func TestOpenEmptyDeclaredRepo(t *testing.T) {
	d := t.TempDir()

	txt := `
; Setting Path does not affect the Path attribute of the repository.
Path=/potato
	`

	if err := os.WriteFile(filepath.Join(d, "netota.repo"), []byte(txt), 0o600); err != nil {
		t.Fatalf("cannot write file: %s", err)
	}

	r, err := fsrepo.Open(d)
	if err != nil {
		t.Fatalf("cannot open repo: %s", err)
	}

	if r.Path != d {
		t.Fatalf("r.Path mismatch: got %s, expected %s", r.Path, d)
	}
}

func TestSmoke(t *testing.T) {
	d := t.TempDir()

	if err := os.WriteFile(filepath.Join(d, "netota.repo"), nil, 0o600); err != nil {
		t.Fatalf("cannot write file: %s", err)
	}

	// The router package has three streams: pre-release (closed), stable, beta.
	// Both beta and stable have one archive inside.

	if err := os.WriteFile(filepath.Join(d, "router.package"), nil, 0o600); err != nil {
		t.Fatalf("cannot write file: %s", err)
	}

	if err := os.Mkdir(filepath.Join(d, "router.package.d"), 0o700); err != nil {
		t.Fatalf("cannot create directory: %s", err)
	}

	if err := os.WriteFile(filepath.Join(d, "router.package.d/pre-release.stream"), nil, 0o600); err != nil {
		t.Fatalf("cannot write file: %s", err)
	}

	routerPreReleaseStreamText := `
[Stream]
Name=pre-release
Closed=true
ReplacedBy=stable
`
	if err := os.WriteFile(filepath.Join(d, "router.package.d/pre-release.stream"), []byte(routerPreReleaseStreamText), 0o600); err != nil {
		t.Fatalf("cannot write file: %s", err)
	}

	if err := os.WriteFile(filepath.Join(d, "router.package.d/stable.stream"), nil, 0o600); err != nil {
		t.Fatalf("cannot write file: %s", err)
	}

	if err := os.Mkdir(filepath.Join(d, "router.package.d/stable.stream.d"), 0o700); err != nil {
		t.Fatalf("cannot create directory: %s", err)
	}

	routerStableArchiveText := `
[Archive]
Name=router-1.0.0-aarch64.bundle
Size=100000000
Type=rauc.bundle
Format=rauc.bundle.plain
[Checksums]
Sha256=1234
[Compatible]
Arch=aarch64
[Content]
Type=sysota.system-image
Format=squashfs.v4.xz
Size=120000000
Variant=release
[Download]
URL=https://example.org/router-1.0.0-aarch64.bundle
[Package]
Name=router
Version=1.0.0
[Source]
Revision=deadbeef
Aliases=v1.0.0
`
	if err := os.WriteFile(filepath.Join(d, "router.package.d/stable.stream.d/router-1.0.0-aarch64.bundle.archive"), []byte(routerStableArchiveText), 0o600); err != nil {
		t.Fatalf("cannot write file: %s", err)
	}

	if err := os.WriteFile(filepath.Join(d, "router.package.d/beta.stream"), nil, 0o600); err != nil {
		t.Fatalf("cannot write file: %s", err)
	}

	if err := os.Mkdir(filepath.Join(d, "router.package.d/beta.stream.d"), 0o700); err != nil {
		t.Fatalf("cannot create directory: %s", err)
	}

	routerBetaArchiveText := `
[Archive]
Name=router-1.1.0-aarch64.bundle
Size=100000000
Type=rauc.bundle
Format=rauc.bundle.plain
[Checksums]
Sha256=5678
[Compatible]
Arch=aarch64
[Content]
Type=sysota.system-image
Format=squashfs.v4.xz
Size=120000000
Variant=debug
[Download]
URL=https://example.org/router-1.1.0-aarch64.bundle
[Package]
Name=router
Version=1.1.0
[Source]
Revision=cafecafe
Aliases=v1.1.0
`
	if err := os.WriteFile(filepath.Join(d, "router.package.d/beta.stream.d/router-1.1.0-aarch64.bundle.archive"), []byte(routerBetaArchiveText), 0o600); err != nil {
		t.Fatalf("cannot write file: %s", err)
	}

	// The gateway package has just one stream and one package in the nightly stream.

	if err := os.WriteFile(filepath.Join(d, "gateway.package"), nil, 0o600); err != nil {
		t.Fatalf("cannot write file: %s", err)
	}

	if err := os.Mkdir(filepath.Join(d, "gateway.package.d"), 0o700); err != nil {
		t.Fatalf("cannot create directory: %s", err)
	}

	if err := os.WriteFile(filepath.Join(d, "gateway.package.d/nightly.stream"), nil, 0o600); err != nil {
		t.Fatalf("cannot write file: %s", err)
	}

	if err := os.Mkdir(filepath.Join(d, "gateway.package.d/nightly.stream.d"), 0o700); err != nil {
		t.Fatalf("cannot create directory: %s", err)
	}

	gatewayNightlyArchiveText := `
	[Archive]
	Name=gateway-0.1.0-aarch64.bundle
	Size=100000000
	Type=rauc.bundle
	Format=rauc.bundle.plain
	[Checksums]
	Sha256=1357
	[Compatible]
	Arch=aarch64
	[Content]
	Type=sysota.system-image
	Format=squashfs.v4.xz
	Size=120000000
	Variant=debug
	[Download]
	URL=https://example.org/gateway-0.1.0-aarch64.bundle
	[Package]
	Name=gateway
	Version=0.1.0
	[Source]
	Revision=fafafafafafa
`
	if err := os.WriteFile(filepath.Join(d, "gateway.package.d/nightly.stream.d/gateway-0.1.0-aarch64.bundle.archive"), []byte(gatewayNightlyArchiveText), 0o600); err != nil {
		t.Fatalf("cannot write file: %s", err)
	}

	// The speaker package has no streams or archives yet.

	if err := os.WriteFile(filepath.Join(d, "speaker.package"), nil, 0o600); err != nil {
		t.Fatalf("cannot write file: %s", err)
	}

	// Let's open this repository.

	r, err := fsrepo.Open(d)
	if err != nil {
		t.Fatalf("cannot open repo: %s", err)
	}

	pkgs := r.QueryPackages(context.Background())
	if len(pkgs) != 3 {
		t.Fatalf("unexpected packages: got %d, expected 3", len(pkgs))
	}

	sort.Slice(pkgs, func(i, j int) bool {
		return pkgs[i].Name < pkgs[j].Name
	})

	var names [3]string
	for i := range pkgs {
		names[i] = pkgs[i].Name
	}

	if !slices.StringEqual(names[:], []string{"gateway", "router", "speaker"}) {
		t.Fatalf("unexpected order of packages: %s", names)
	}

	// TODO: explore the returned values deeper.

	streams := r.QueryStreams(context.Background())
	if len(streams) != 4 {
		t.Fatalf("unexpected streams: got %d, expected 4", len(streams))
	}

	archives := r.QueryArchives(context.Background())
	if len(archives) != 3 {
		t.Fatalf("unexpected archives: got %d, expected 3", len(archives))
	}
}
