// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.

package fsrepo

import (
	"errors"
	"fmt"
	"os"
	"strings"

	"gitlab.com/zygoon/go-ini"
)

var (
	errEmptyArchiveName    = errors.New("archive name cannot be empty")
	errZeroArchiveSize     = errors.New("archive size cannot be zero")
	errEmptyPackageName    = errors.New("package name cannot be empty")
	errEmptyPackageVersion = errors.New("package version cannot be empty")
	errEmptySourceRevision = errors.New("source revision cannot be empty")
)

// archiveFile describes a single downloadable file with internal content.
//
// archiveFile must have non-empty, name, size, package name, package version and
// source ID.
type archiveFile struct {
	Path string // Location of the .archive file

	ArchiveName   string `ini:"Name,[Archive],omitempty"`
	ArchiveSize   int64  `ini:"Size"`
	ArchiveType   string `ini:"Type"`
	ArchiveFormat string `ini:"Format"`

	ChecksumSha256 string `ini:"Sha256,[Checksums],omitempty"`
	// other checksum types can be added explicitly.

	CompatibleArch    string `ini:"Arch,[Compatible]"`
	CompatibleSubArch string `ini:"SubArch,omitempty"`
	CompatibleMachine string `ini:"Machine,omitempty"`
	CompatibleDefault bool   `ini:"Default,omitempty"`

	ContentType    string `ini:"Type,[Content]"`
	ContentFormat  string `ini:"Format"`
	ContentSize    int64  `ini:"Size"`
	ContentVariant string `ini:"Variant"`

	DownloadURL    string `ini:"URL,[Download]"`
	DownloadMetric int    `ini:"Metric,omitempty"`

	// TODO: allow defining multiple download offers
	/*
		DownloadOffers []struct {
			URL    string `ini:"URL"`
			Metric int    `ini:"Metric"`
		} `ini:"[Download],section`
	*/

	PackageName    string `ini:"Name,[Package]"`
	PackageVersion string `ini:"Version"`

	SourceRevision string     `ini:"Revision,[Source]"`
	SourceAliases  stringList `ini:"Aliases,omitempty"`
}

// stringList is a space-separated list of strings.
type stringList []string

// UnmarshalText implements encoding.TextUnmarshaler.
func (sa *stringList) UnmarshalText(text []byte) error {
	*sa = strings.Fields(string(text))

	return nil
}

// MarshalText implements encoding.TextMarshaler.
func (sa stringList) MarshalText() ([]byte, error) {
	return []byte(strings.Join([]string(sa), " ")), nil
}

func openArchive(path string) (*archiveFile, error) {
	ar := archiveFile{Path: path}

	if err := ar.loadIni(); err != nil {
		return nil, fmt.Errorf("cannot load archive %q: %w", path, err)
	}

	if err := ar.computeDefaults(); err != nil {
		return nil, fmt.Errorf("cannot compute defaults of archive %q: %w", path, err)
	}

	if err := ar.verify(); err != nil {
		return nil, fmt.Errorf("cannot verify archive %q: %w", path, err)
	}

	return &ar, nil
}

func (ar *archiveFile) loadIni() error {
	f, err := os.Open(ar.Path)
	if err != nil {
		return err
	}

	defer func() {
		_ = f.Close()
	}()

	return ini.NewDecoder(f).Decode(ar)
}

func (ar *archiveFile) computeDefaults() error {
	if ar.ArchiveName == "" {
		ar.ArchiveName = baseWithoutExt(ar.Path)
	}

	return nil
}

func (ar *archiveFile) verify() error {
	if ar.ArchiveName == "" {
		return errEmptyArchiveName
	}

	if ar.ArchiveSize == 0 {
		return errZeroArchiveSize
	}

	if ar.PackageName == "" {
		return errEmptyPackageName
	}

	if ar.PackageVersion == "" {
		return errEmptyPackageVersion
	}

	if ar.SourceRevision == "" {
		return errEmptySourceRevision
	}

	return nil
}
