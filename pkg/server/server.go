// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.

// Package server implements the NetOTA request handler.
//
// Available API endpoints:
//
// GET /api/v1/package/$name
//
// List streams of a given package.
//
// The response contains information relevant to making the choice of which
// stream to select. The selection can be done either interactively, by a human,
// or automatically, by a management agent. The supplied information contains
// source identifier, version, replacement channel for closed channels, source
// aliases such as git tags or branch names.
//
// GET /api/v1/package/$package/$stream
//
// List archives of a given package and stream.
//
// The response contains information about available archives and their download
// offers. Each archive is accompanied by additional information which describes
// the type and format of both the archive itself and the content inside.
//
// Additional endpoints may be available over time but no backwards incompatible
// changes shall be made made to existing endpoints.
package server

import (
	"encoding/json"
	"fmt"
	"net/http"
	"path"
	"strings"
	"sync"

	"gitlab.com/zygoon/netota"
)

const (
	hdrContentType = "Content-Type"
	typeTextJSON   = "text/json"
)

// Handler implements NetOTA HTTP request handler.
//
// See documentation of the server package for details.
type Handler struct {
	packages packageHandler
}

// NewHandler returns a new HTTP handler exposing the given repository.
func NewHandler(repo netota.Repo) *Handler {
	return &Handler{packages: packageHandler{repo: repo}}
}

// SetRepo atomically replaces the repository served by the handler.
func (h *Handler) SetRepo(r netota.Repo) {
	h.packages.setRepo(r)
}

func (h *Handler) ServeHTTP(res http.ResponseWriter, req *http.Request) {
	var head string

	head, req.URL.Path = shiftPath(req.URL.Path)

	switch head {
	case "api":
		head, req.URL.Path = shiftPath(req.URL.Path)
		switch head {
		case "v1":
			head, req.URL.Path = shiftPath(req.URL.Path)
			switch head {
			case "package":
				h.packages.ServeHTTP(res, req)
				return
			}
		}
	}

	res.WriteHeader(http.StatusNotFound)
}

// packageHandler responds to /$package and /$package/$stream.
type packageHandler struct {
	m    sync.RWMutex
	repo netota.Repo
}

// setRepo atomically replaces the repository offered by the handler.
func (h *packageHandler) setRepo(repo netota.Repo) {
	h.m.Lock()
	defer h.m.Unlock()

	h.repo = repo
}

func (h *packageHandler) servePackage(res http.ResponseWriter, req *http.Request, pkgName string) {
	h.m.RLock()
	defer h.m.RUnlock()

	pkgs := h.repo.QueryPackages(req.Context(), h.repo.PackageName(pkgName))
	if len(pkgs) == 0 {
		http.NotFound(res, req)
		return
	}

	pkg := pkgs[0]

	streams := h.repo.QueryStreams(req.Context(), h.repo.PackageName(pkgName))

	pkg.Streams = make(map[string]*netota.Stream)
	for _, stream := range streams {
		pkg.Streams[stream.Name] = stream
	}

	data, err := json.Marshal(pkg)
	if err != nil {
		http.Error(res, fmt.Sprintf("cannot marshal package: %v\n", err), http.StatusInternalServerError)

		return
	}

	res.Header().Add(hdrContentType, typeTextJSON)
	res.WriteHeader(http.StatusOK)
	_, _ = res.Write(data)
}

func (h *packageHandler) servePackageStream(res http.ResponseWriter, req *http.Request, pkgName, streamName string) {
	h.m.RLock()
	defer h.m.RUnlock()

	streams := h.repo.QueryStreams(req.Context(), h.repo.PackageName(pkgName), h.repo.StreamName(streamName))
	if len(streams) == 0 {
		http.NotFound(res, req)
		return
	}

	stream := streams[0]

	stream.Archives = h.repo.QueryArchives(req.Context(), h.repo.PackageName(pkgName), h.repo.StreamName(streamName))

	data, err := json.Marshal(stream)
	if err != nil {
		http.Error(res, fmt.Sprintf("cannot marshal stream: %v\n", err), http.StatusInternalServerError)

		return
	}

	res.Header().Add(hdrContentType, typeTextJSON)
	res.WriteHeader(http.StatusOK)
	_, _ = res.Write(data)
}

func (h *packageHandler) ServeHTTP(res http.ResponseWriter, req *http.Request) {
	if req.Method != http.MethodGet {
		res.WriteHeader(http.StatusBadRequest)
		return
	}

	var pkgName, streamName string

	pkgName, req.URL.Path = shiftPath(req.URL.Path)
	streamName = req.URL.Path[1:]

	if streamName == "" {
		h.servePackage(res, req, pkgName)
	} else {
		h.servePackageStream(res, req, pkgName, streamName)
	}
}

func shiftPath(p string) (head, tail string) {
	p = path.Clean("/" + p)

	i := strings.Index(p[1:], "/") + 1

	if i <= 0 {
		return p[1:], "/"
	}

	return p[1:i], p[i:]
}
