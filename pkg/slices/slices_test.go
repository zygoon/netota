// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.

package slices_test

import (
	"testing"

	"gitlab.com/zygoon/netota/pkg/slices"
)

func TestSlicesEqual(t *testing.T) {
	if !slices.StringEqual(nil, nil) {
		t.Fatalf("nil slices are not equal")
	}

	if !slices.StringEqual(nil, []string{}) {
		t.Fatalf("nil and empty slice is not equal")
	}

	if !slices.StringEqual([]string{"a", "b", "c"}, []string{"a", "b", "c"}) {
		t.Fatalf("expected slices to be equal")
	}

	if slices.StringEqual([]string{"a"}, []string{"b"}) {
		t.Fatal("different string slices are incorrectly equal")
	}

	if slices.StringEqual([]string{"a"}, []string{"a", "b"}) {
		t.Fatal("slices of different length are incorrectly equal")
	}
}
