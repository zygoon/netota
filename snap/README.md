<!--
SPDX-License-Identifier: Apache-2.0
SPDX-FileCopyrightText: Huawei Inc.
-->

# The netota snap

[![netota](https://snapcraft.io/netota/badge.svg)](https://snapcraft.io/netota)

This directory contains the snap packaging for the `netota` tool. Any changes
made to the official upstream git repository at
https://gitlab.com/zygoon/netota, are automatically built by Launchpad and
published to the snap store. The packaging recipe is at
https://launchpad.net/~zyga/netota/+snap/netota

The built snap is published to the `latest/edge` channel. Tagged releases
should be auto-built or remote-built and published to specific channels
manually.

## Development and local testing

You can build the snap package locally with `snapcraft`. See snapcraft
documentation for basic information about how to do this.

## Repository

The snap package maintains a repository at `$SNAP_COMMON/repository`. The snap
install hook copies a sample repository to help users with discovery.

## Configuration

The snap package uses the snap configuration system to store the listen address
of the netotad service. Use `snap set netota address=...` to set the address to
listen on.
