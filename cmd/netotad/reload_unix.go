// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.

//go:build !windows
// +build !windows

package main

import (
	"os"
	"os/signal"
	"syscall"

	"golang.org/x/sys/unix"
)

func registerReload(reloadCh chan os.Signal) (restore func()) {
	signal.Notify(reloadCh, syscall.SIGUSR1)

	return func() {
		signal.Reset(unix.SIGUSR1)
	}
}
