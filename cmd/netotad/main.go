// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.

// Package main contains the implementation of NetOTA service.
package main

import (
	"context"
	"errors"
	"flag"
	"io"
	"log"
	"net/http"
	"os"

	"gitlab.com/zygoon/go-cmdr"

	"gitlab.com/zygoon/netota/pkg/fsrepo"
	"gitlab.com/zygoon/netota/pkg/server"
)

func main() {
	cmdr.RunMain(&netotadCmd{}, os.Args)
}

type options struct {
	Addr     string
	RepoPath string
}

type repoMissingError struct{}

func (repoMissingError) Error() string {
	return "repository path missing"
}

func (repoMissingError) ExitCode() int {
	return 64 // EX_USAGE
}

func parseOpts(stderr io.Writer, args []string) (*options, error) {
	fs := flag.NewFlagSet("netotad", flag.ContinueOnError)
	fs.SetOutput(stderr)
	addr := fs.String("listen-addr", ":8080", "Network address to listen on")
	repoPath := fs.String("repo", "", "Repository to serve")

	err := fs.Parse(args)
	if err != nil {
		return nil, err
	}

	if *repoPath == "" {
		return nil, repoMissingError{}
	}

	return &options{Addr: *addr, RepoPath: *repoPath}, nil
}

type netotadCmd struct{}

func (*netotadCmd) Run(ctx context.Context, args []string) error {
	_, _, stderr := cmdr.Stdio(ctx)
	opts, err := parseOpts(stderr, args)

	if err != nil {
		return err
	}

	repo, err := fsrepo.Open(opts.RepoPath)
	if err != nil {
		return err
	}

	handler := server.NewHandler(repo)

	// Reload the repository when SIGUSR1 arrives.
	reloadCh := make(chan os.Signal, 1)
	defer close(reloadCh)

	restore := registerReload(reloadCh)
	defer restore()

	go func() {
		for {
			select {
			case <-ctx.Done():
				return
			case <-reloadCh:
				repo, err := fsrepo.Open(opts.RepoPath)
				if err != nil {
					log.Printf("Cannot hot-reload repository: %v\n", err)
					continue
				}

				log.Printf("Repository reloaded\n")
				handler.SetRepo(repo)
			}
		}
	}()

	srv := &http.Server{Addr: opts.Addr, Handler: handler}

	go func() {
		if err := srv.ListenAndServe(); err != nil {
			if !errors.Is(err, http.ErrServerClosed) {
				log.Printf("server error: %v\n", err)
			}
		}
	}()

	// Wait until notify context is cancelled by an interrupt.
	<-ctx.Done()

	// Gracefully shut down the server.
	if err := srv.Shutdown(context.Background()); err != nil {
		log.Printf("shutdown error: %v\n", err)
	}

	// ctx.Err is always context.Canceled so don't bother returning it.
	return nil
}
