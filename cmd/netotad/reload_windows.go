// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.

//go:build windows
// +build windows

package main

import (
	"os"
)

func registerReload(reloadCh chan os.Signal) (restore func()) {
	return func() {}
}
