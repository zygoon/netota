// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.

// Package main implements the netotactl executable.
package main

import (
	"context"
	"errors"
	"flag"
	"fmt"
	"os"

	"gitlab.com/zygoon/go-cmdr"
	"gitlab.com/zygoon/go-cmdr/router"
	"gopkg.in/yaml.v3"

	"gitlab.com/zygoon/netota"
)

var (
	errServerAddressMissing = errors.New("please provide server address with -addr")
	errPackageNameMissing   = errors.New("please provide package name")
	errStreamNameMissing    = errors.New("please provide stream name")
)

func main() {
	cmdr.RunMain(&NetotactlCmd{}, os.Args)
}

// Options describes global options shared across commands.
type Options struct {
	Addr string
}

const (
	packageCmdName = "package"
	streamCmdName  = "stream"
)

// NetotactlCmd implements the "netotactl" command.
type NetotactlCmd struct{}

// Run routes execution to one of the sub-commands of "netotactl".
func (cmd *NetotactlCmd) Run(ctx context.Context, args []string) error {
	opts := Options{}

	fs := flag.NewFlagSet("netotactl [-addr ADDRESS] COMMAND", flag.ContinueOnError)
	fs.StringVar(&opts.Addr, "addr", "", "Address of the NetOTA server to connect to ")

	if err := fs.Parse(args); err != nil {
		return err
	}

	r := router.Cmd{
		Name: "netotactl",
		Commands: map[string]cmdr.Cmd{
			packageCmdName: PackageCmd{},
			streamCmdName:  StreamCmd{},
		},
	}

	return r.Run(NewContext(ctx, &opts), fs.Args())
}

type optionsKey struct{}

// NewContext returns a new Context that carries value o.
func NewContext(ctx context.Context, o *Options) context.Context {
	return context.WithValue(ctx, optionsKey{}, o)
}

// FromContext returns the Options value stored in ctx, if any.
func FromContext(ctx context.Context) (*Options, bool) {
	u, ok := ctx.Value(optionsKey{}).(*Options)
	return u, ok
}

// clientMixin provides access to netota.Client.
type clientMixin struct{}

// Client returns a client with the address of NetOTA server.
//
// Client panics if the context does not contain global options.
func (clientMixin) Client(ctx context.Context) (*netota.Client, error) {
	opts, _ := FromContext(ctx)
	if opts == nil {
		panic("options not supplied through the context")
	}

	if opts.Addr == "" {
		return nil, errServerAddressMissing
	}

	return &netota.Client{Addr: opts.Addr}, nil
}

// PackageCmd implements "netotactl package" command.
type PackageCmd struct {
	clientMixin
}

// OneLiner provides a help message to the command router.
func (cmd PackageCmd) OneLiner() string {
	return "Display information about a specific package"
}

// Run executes the "netotactl package" command with given arguments.
func (cmd PackageCmd) Run(ctx context.Context, args []string) error {
	_, stdout, stderr := cmdr.Stdio(ctx)
	fs := flag.NewFlagSet(cmdr.Name(ctx), flag.ContinueOnError)
	fs.SetOutput(stderr)
	fs.Usage = func() {
		_, _ = fmt.Fprintf(fs.Output(), "Usage: %s PACKAGE\n", fs.Name())
	}

	if err := fs.Parse(args); err != nil {
		return err
	}

	pkgName := fs.Arg(0)
	if pkgName == "" {
		fs.Usage()

		return errPackageNameMissing
	}

	cli, err := cmd.Client(ctx)
	if err != nil {
		return err
	}

	defer cli.CloseIdleConnections()

	pkg, err := cli.Package(ctx, pkgName)
	if err != nil {
		return fmt.Errorf("cannot fetch package: %w", err)
	}

	if err := yaml.NewEncoder(stdout).Encode(pkg); err != nil {
		return fmt.Errorf("cannot format package as YAML: %w", err)
	}

	return nil
}

// StreamCmd implements "netotactl stream" command.
type StreamCmd struct {
	clientMixin
}

// OneLiner provides a help message to the command router.
func (cmd StreamCmd) OneLiner() string {
	return "Display contents of a specific stream"
}

// Run executes the "netotactl stream" command with given arguments.
func (cmd StreamCmd) Run(ctx context.Context, args []string) error {
	_, stdout, stderr := cmdr.Stdio(ctx)
	fs := flag.NewFlagSet(cmdr.Name(ctx), flag.ContinueOnError)
	fs.SetOutput(stderr)

	fs.Usage = func() {
		_, _ = fmt.Fprintf(fs.Output(), "Usage: %s PACKAGE STREAM\n", fs.Name())
	}

	if err := fs.Parse(args); err != nil {
		return err
	}

	pkgName := fs.Arg(0)
	if pkgName == "" {
		fs.Usage()

		return errPackageNameMissing
	}

	streamName := fs.Arg(1)
	if streamName == "" {
		fs.Usage()

		return errStreamNameMissing
	}

	cli, err := cmd.Client(ctx)
	if err != nil {
		return err
	}

	defer cli.CloseIdleConnections()

	stream, err := cli.Stream(ctx, pkgName, streamName)
	if err != nil {
		return fmt.Errorf("cannot fetch stream stream: %w", err)
	}

	if err := yaml.NewEncoder(stdout).Encode(stream); err != nil {
		return fmt.Errorf("cannot format stream as YAML: %w", err)
	}

	return nil
}
