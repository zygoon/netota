<!--
SPDX-License-Identifier: Apache-2.0
SPDX-FileCopyrightText: Huawei Inc.
-->

# NetOTA - suite of network services for SysOTA

NetOTA provides a pair of utilities for working with SysOTA repositories.

## Installation

There are no binary releases available yet. To build NetOTA you will need the
[Go toolchain](https://go.dev/dl/). To build and install `netotad` please run:

```sh
go install gitlab.com/zygoon/netota/cmd/netotad@latest
```

You can find the resulting binary in the `bin` subdirectory of the `GOPATH`
directory for your platform. Run `go env GOPATH` to find it. Depending on how
your platform is set up, the built binary may be on your path already.

Note that you do not need to clone the Git repository to do this. If you have
already cloned the repository you can install local builds with:

```sh
go install ./...
```

Alternatively you can build a specific binary with:

```sh
go build ./cmd/netotad
go build ./cmd/netotactl
```

## netotactl - query utility

The `netotactl` utility can be used to query a repository exposed by `netotad`.

Refer to `netotactl --help` for details.

## netotad -  package stream meta-data service

The `netotad` service exposes a local NetOTA repository over http.

Refer to `netotad --help` for details.

To reload the repository with zero downtime, send `SIGUSR1` to a running
`netotad` process. The repository is only replaced if it has been loaded
correctly and is self-consistent.

# Repository Format

The repository is a directory structure comprised of *packages*, *streams* and
*archives*. Each entity is described by a definition file suffixed with
`.package`, `.stream` and `.archive`, respectively. In addition, the root of the
repository is identified by the presence of a `netota.repo` file.

At the top level a repository contains any number of `.package` files and their
contents in a `.package.d` directories. A package directory contains any number
of `.stream` and `.stream.d` directories. Each stream contains any number of
`.archive` files.

## netota.repo file

Each repository can be described by a single `netota.repo` file, which defines
the subtree with packages, streams and archives.

## .package files

Packages are names or identifiers for available content.

Each package is defined by a file with the  `.package` suffix placed at root of
the repository.

Package definition files have the following structure.

### [Package] section

#### Name= field

Optional name of the package.

Package names default to the name of the file defining it with the `.package`
suffix stripped.

## .stream files

Streams describe logical distribution points for archives of a given package. A
package may offer any number of streams simultaneously and clients remember the
stream they wish to follow for that particular package.

Clients cannot request archives other than those published in a stream. If
multiple versions, stability levels or other variants need to exist at the same
time those must be represented by multiple streams with distinct names.

Clients track the source revision of each package they subscribe to. Any time a
stream offers a compatible archive with different source revision, the client
recognizes this as a possibility of performing the update. Source revisions are
not ordered.

Each stream is defined by a file with the `.stream` suffix placed in the
`.package.d/` directory of the package the stream belongs to.

Stream definition files have the following structure.

### [Stream] section

The `[Stream]` section describes the properties of the stream.

#### Name= field

Optional name of the stream.

Stream names default to the name of the stream definition file with the
`.stream` suffix stripped and all the `-` characters replaced with `/`. This
allows streams to define a logical hierarchy without imposing it on the file
system.

#### Closed= field

An optional boolean flag marking the stream as closed.

When a client subscribes to a closed stream, it may choose to move to a
replacement stream if one is available.

#### ReplacedBy= field

Optional name of the replacement stream to move to for a closed stream.

When non-empty it must be the name of a non-closed stream in the same package.

## .archive files

Archives describe individual downloadable files with specific content. Archive
files must be placed in a specific stream's `.stream.d/` directory.

Each archive file describes the archiving "envelope", the actual content as well
as systems compatible with the content. This allows the same logical content to
be provided for different architectures, for different machines or even for the
same machine but with different compression or archiving properties.

Archives may be also used to convey source code corresponding to binaries.

All archives published in a given stream at a given time are derived from the
same source revision, which implies the same package name and package version.

Each archive is defined by a file with the `.archive` suffix placed in the
`.stream.d/` directory of the stream the archive belongs to.

### [Archive] section

The `[Archive]` section describes the archive itself.

#### Name= field

Name of the archive file as it should appear on disk.

#### Size= field

Size of the file offered for download in bytes.

#### Type= field

Type of the archive.

Currently the only defined archive type is `rauc.bundle` which corresponds to a
RAUC bundle file.

#### Format= field

Format of the archive.

Currently the only defined archive types are `rauc.bundle.plain` and
`rauc.bundle.verity` which correspond to particular formats of RAUC bundles.
Not all kernels support the `verity` bundle type and not all clients may wish
to use the less secure `plain` bundle types.

### [Checksums] section

The `[Checksums]` section contains verification checksums of particular types.

#### Sha256= field

SHA-256 checksum of the archive file as a string.

### [Compatible] section

The `[Compatible]` section describes systems which are compatible with the
archived content. Not all fields need to be set.

#### Arch= field

CPU architecture required by the content. Currently the following architectures
are defined.

| **Name**  | **Description**                     |
|-----------|-------------------------------------|
| `noarch`  | Architecture-independent content    |
| `x86`     | Content for 32bit Intel/AMD systems |
| `x86-64`  | Content for 64bit Intel/AMD systems |
| `arm`     | Content for 32bit ARM systems       |
| `aarch64` | Content for 64bit ARM systems       |
| `riscv32` | Content for 32bit RISC-V systems    |
| `riscv64` | Content for 64bit RISC-V systems    |

#### SubArch= field

Optional name of the sub-architecture further narrowing the set of compatible
systems. No values are defined for this field yet.

#### Machine= field

Optional name of the specific machine even further narrowing the set of
compatible systems.

#### Default= field

The default field aids in archive selection. This boolean field may be set to
true for each tuple (Arch, SubArch, Machine). If multiple archives are
available, for example offering different compression options, the default
archive is used in absence of device-specific preferences.

### [Content] section

The `[Content]` section describes the content of the archive.

#### Type= field

Type of the content stored within the archive.

Currently the only defined content type is `sysota.system-image` which is a
SysOTA `system` image for updating in A/B slots.

#### Format= field

Format of the content stored within the archive.

Currently the only defined content formats are `squashfs.v4.xz` which is a
squashfs file system, version 4, with XZ compression and `tar.gz` for the
classic tar archive.

#### Variant= field

Optional variant of the content. This field may be used to offer multiple
archives with the same logical content but distinct build options.

Currently the only defined content variants are `release` and `test`.

#### Size= field

Size of the content stored in the archive in bytes. This may be smaller or
larger than Archive size depending on compression types and other factors. It
serves as a hint for clients to avoid retrieving content which could not be used
due to space constraints.

### [Download] section

The `[Download]` section describes download offers for the archive. At the
moment only one entry is supported.

#### URL= field

The URL of the offered file. Currently only one such field may be used in any
given download section.

#### Metric= field

The optional metric number. When multiple offers exist, the client should prefer
the one with the lowest metric value.

### [Package] section

The `[Package]` section ties the archive to a specific package. This field
exists as a mechanism to avoid mistakes if the package file is copied or
otherwise handled incorrectly.

#### Name= field

Name of the package.

The value must be identical to the name of the package.

#### Version= field

The version of the package.

The version is free-form and carries no meaning, except as an information for
people. The version, similar to package name, exists as a mechanism to avoid
mistakes if the package file is copied or otherwise handled incorrectly.

All the archives in a given stream must have the exact same package version.

### [Source] section

The `[Source]` section describes the properties of the source which was used to
create the archive.

#### Revision= field

Revision is a mandatory string identifying the particular set of source files
and their content. Where possible this should be a Git commit ID or other
similar content-derived identifier.

All the archives in a given stream must have the exact same source revision.

#### Aliases= field

Aliases is an optional space-separated list of aliases that may be used
interchangeably with the source revision. Where possible this should be a list
of Git tags or branch names.

