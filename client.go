// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.

package netota

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"
)

// Client is a http.Client tuned for interacting with NetOTA HTTP APIs.
//
// A client knows the address of the server which is implicitly used for each
// request.
type Client struct {
	http.Client

	Addr string // The address including the protocol and port number.
}

// Package requests information about package and the streams therein.
func (c *Client) Package(ctx context.Context, pkgName string) (*Package, error) {
	pkg := Package{Name: pkgName}
	if err := c.query(ctx, &pkg); err != nil {
		return nil, err
	}

	return &pkg, nil
}

// Stream requests information about package stream and the archives therein.
func (c *Client) Stream(ctx context.Context, pkgName, streamName string) (*Stream, error) {
	stream := Stream{PackageName: pkgName, Name: streamName}
	if err := c.query(ctx, &stream); err != nil {
		return nil, err
	}

	return &stream, nil
}

// queryable is an entity which can be queried.
//
// Entity has a kind, which is used in error messages, and a path suffix which
// encodes the location of the queryable entity on the server. Before performing
// the query, create a concrete entity, fill in the fields needed to compute the
// path suffix and then perform the query.
type queryable interface {
	kind() entityKind
	pathSuffix() string
}

// query performs the request, verifies and decodes the response.
//
// Pass a queryable object with enough fields set to compute the path suffix.
// The exact set differs per type.
func (c *Client) query(ctx context.Context, q queryable) error {
	req, err := http.NewRequestWithContext(ctx, http.MethodGet, c.Addr, nil)
	if err != nil {
		return &queryError{Kind: q.kind(), Err: err}
	}

	req.URL.Path += q.pathSuffix()

	resp, err := c.Client.Do(req)
	if err != nil {
		return &queryError{Kind: q.kind(), Err: err}
	}

	defer func() {
		_ = resp.Body.Close()
	}()

	if resp.StatusCode != http.StatusOK {
		return &queryError{Kind: q.kind(), Err: &unexpectedStatus{StatusCode: resp.StatusCode}}
	}

	if t := resp.Header.Get("Content-Type"); t != "text/json" {
		return &queryError{Kind: q.kind(), Err: &unexpectedContentType{ContentType: t}}
	}

	if err := json.NewDecoder(resp.Body).Decode(q); err != nil {
		return &queryError{Kind: q.kind(), Err: err}
	}

	return nil
}

// entityKind identifies the kind of entity.
type entityKind string

const (
	// kindPackage is the entity of the Package type.
	kindPackage entityKind = "package"
	// kindStream is the entity of the Stream type.
	kindStream entityKind = "stream"
)

// queryError wraps all errors related to performing queries.
type queryError struct {
	Kind entityKind
	Err  error
}

// Error returns a human readable error message.
func (e *queryError) Error() string {
	return fmt.Sprintf("cannot query %s: %v", e.Kind, e.Err)
}

// Unwrap returns the wrapped error.
func (e *queryError) Unwrap() error {
	return e.Err
}

// unexpectedStatus records unexpected status of the response.
type unexpectedStatus struct {
	StatusCode int
}

// Error returns a human readable error message.
func (e unexpectedStatus) Error() string {
	return fmt.Sprintf("unexpected status code: %d", e.StatusCode)
}

// unexpectedContentType records unexpected content type of the response.
type unexpectedContentType struct {
	ContentType string
}

// Error returns a human readable error message.
func (e *unexpectedContentType) Error() string {
	return fmt.Sprintf("unexpected content type: %s", e.ContentType)
}
