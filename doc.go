// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.

// Package netota implements a repository of packages for sysota.
//
// The repository is set ot of packages. Package is a named entity. Stream is a
// path-like object for publishing archives of a package. Archive is a file
// with a format and content that is related to a package.
//
// Rules of thumb that may help seasoned developers attain the right mindset.
//
// 1) Packages are identifiers, not files.
//
// 1.1) Packages are visible to users and should have sensible names.
//
// 1.2) Packages are neither "binary packages" nor "source packages".
//
// 1.3) Packages offer streams where archives are published.
//
// 2) Streams are path-like identifiers, akin to directories.
//
// 2.1) Streams are visible to users and should have sensible names.
//
// 2.2) Streams allow a single package to offer multiple versions or stability
// levels.
//
// 2.3) A single stream offers a single version of a package as one or more
// archives.
//
// 2.4) A stream may be closed.
//
// 2.5) A closed stream may pick name a non-closed replacement stream.
//
// 3) Archives are files with content.
//
// 3.1) Archive has a source revision which is assigned by the creator of the
// archive. The revision may be a git hash, a perforce changelist number or
// anything else that is useful in identifying the content.
//
// 3.2) Archive names contain some information but do not have a fixed format.
//
// 3.3) The same content may be archived differently.
//
// 3.4) The same source may be built many times to many archives.
//
// 3.5) Archives know their CPU architecture and sub-architecture.
//
// 3.6) Archives may be architecture-independent.
//
// 3.7) Archives may be used to publish source code.
//
// 3.8) Source archives are entirely optional.
//
// 3.9) Archive has specific type and format.
//
// 3.10) Archive content has specific type, format and variant.
//
// 3.11) Each archive may have multiple download offers.
//
// 4) Devices have preferences and constraints.
//
// 4.1) Device knows the packages it is interested in.
//
// 4.2) Device knows the per-package stream name it is interested in.
//
// 4.3) Device knows the per-package source revision it has locally
//
//	whenever the revision stored locally is different from the
//	revision available in the channel, and the device is not
//	managed, the device can update.
//
// 4.4) Device knows its constraints that restrict comaptbile archives.
//
// 4.5) Processor architecture and sub-architecture is a constraint.
//
// 4.6) Kernel feature set is a constraint (file system type, compression type).
//
// 4.7) Other constraints may be encoded (e.g. prefer formats that use less
// memory)
package netota
