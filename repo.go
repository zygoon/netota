// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.

package netota

import (
	"context"
)

// Repo is an interface for package repositories.
type Repo interface {
	PackageName(name string) QueryOption
	StreamName(name string) QueryOption

	QueryPackages(ctx context.Context, opts ...QueryOption) (result []*Package)
	QueryStreams(ctx context.Context, opts ...QueryOption) (result []*Stream)
	QueryArchives(ctx context.Context, opts ...QueryOption) (result []*Archive)
}

// QueryOption is an interface for repository query options.
type QueryOption interface {
	QueryOption()
}
