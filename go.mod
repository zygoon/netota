module gitlab.com/zygoon/netota

go 1.22

require gitlab.com/zygoon/go-ini v1.2.0

require gopkg.in/yaml.v3 v3.0.1

require golang.org/x/sys v0.19.0

require gitlab.com/zygoon/go-cmdr v1.9.0
