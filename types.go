// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.

package netota

import (
	"fmt"
	"time"
)

// Package is a named entity.
//
// Packages may offer any number of streams which provide different versions or
// stability levels of the same package.
type Package struct {
	Name    string             `json:"name" yaml:"name"`
	Streams map[string]*Stream `json:"streams,omitempty" yaml:"streams,omitempty" dbus:"-"`
}

func (pkg *Package) pathSuffix() string {
	return fmt.Sprintf("/api/v1/package/%s", pkg.Name)
}

func (*Package) kind() entityKind {
	return kindPackage
}

// Stream is used to publish package archives.
//
// Name is the name of the stream. Stream names may be constrained but must
// support forward slashes '/'.
//
// Whenever stream content changes the UpdatedOn timestamp is modified. Streams
// can be closed. A closed stream no longer has UpdatedOn timestamp and may have
// a non-empty ReplacedBy to indicate the replacement stream to switch to.
//
// PackageName and PackageVersion identify the name and version of the package
// published in the stream. Version is meant for humans and carries no
// semantics. All streams for a given package have the same PackageName but may
// have different PackageVersion, SourceRevision and SourceAliases.
//
// SourceRevision represents the source identifier of all the archives published to
// the stream. This may be a git revision. SourceRevision is used for debugging. All
// archives in a given channel must have the same SourceRevision. SourceAliases
// represent alternative names or SourceRevision. Those may be git tag names or branch
// names.
type Stream struct {
	Name           string     `json:"name" yaml:"-"`
	IsClosed       bool       `json:"closed,omitempty" yaml:"closed,omitempty"`
	ReplacedBy     string     `json:"replaced-by,omitempty" yaml:"replaced-by,omitempty"`
	UpdatedOnSec   int64      `json:"updated-on-sec,omitempty" yaml:"updated-on-sec,omitempty"` // Seconds since UNIX epoch, wire format.
	PackageName    string     `json:"package-name,omitempty" yaml:"package-name,omitempty"`
	PackageVersion string     `json:"package-version,omitempty" yaml:"package-version,omitempty"`
	SourceRevision string     `json:"source-revision,omitempty" yaml:"source-version,omitempty"`
	SourceAliases  []string   `json:"source-aliases,omitempty" yaml:"source-aliases,omitempty,flow"`
	Archives       []*Archive `json:"archives,omitempty" yaml:"archives,omitempty" dbus:"-"`
}

type yStream struct {
	IsClosed       bool       `yaml:"closed,omitempty"`
	ReplacedBy     string     `yaml:"replaced-by,omitempty"`
	UpdatedOn      time.Time  `yaml:"updated-on,omitempty"`
	PackageName    string     `yaml:"package-name,omitempty"`
	PackageVersion string     `yaml:"package-version,omitempty"`
	SourceRevision string     `yaml:"source-revision,omitempty"`
	SourceAliases  []string   `yaml:"source-aliases,omitempty,flow"`
	Archives       []*Archive `yaml:"archives,omitempty"`
}

// MarshalYAML returns the YAML equivalent of Stream.
func (stream Stream) MarshalYAML() (interface{}, error) {
	y := yStream{
		IsClosed:       stream.IsClosed,
		ReplacedBy:     stream.ReplacedBy,
		UpdatedOn:      stream.UpdatedOn(),
		PackageName:    stream.PackageName,
		PackageVersion: stream.PackageVersion,
		SourceRevision: stream.SourceRevision,
		SourceAliases:  stream.SourceAliases,
		Archives:       stream.Archives,
	}

	return y, nil
}

// UpdatedOn returns the time.Time equivalent of UpdatedOnSec
//
// Dates before UNIX epoch are not supported.
func (stream *Stream) UpdatedOn() (t time.Time) {
	if stream.UpdatedOnSec > 0 {
		t = time.Unix(stream.UpdatedOnSec, 0).UTC()
	}

	return t
}

func (stream *Stream) pathSuffix() string {
	return fmt.Sprintf("/api/v1/package/%s/%s", stream.PackageName, stream.Name)
}

func (*Stream) kind() entityKind {
	return kindStream
}

// Archive represents a content wrapped in an archiving format.
//
// Archives may contain built packages, source archives, system images or
// extensions for other built packages.
//
// ArchiveName is the suggested name of the file as it should appear on disk.
// ArchiveSize is the size of the archive file on disk. ArchiveType is the
// logical description for the kind of archive, for example a RAUC bundle.
// ArchiveFormat is the technical description of the archive, for example a
// "plain" RAUC bundle. ArchiveChecksums is a map of checksums available for the
// image. Those are not replacements for digital signatures embedded in the
// image envelope but may be used for easy verification.
//
// ContentType type is the logical description of the content inside the
// package, for example a SysOTA system image. ContentFormat describes the
// technical format of the content inside the archive, for example a squashfs v4
// with xz compression. ContentVariant can be used to pick a specific build mode
// in systems that support this, for instance a release or a test build.
// ContentSize is the size of the logical package content inside the package.
// Some archives are compressed and the content may be larger than the archive
// itself.
//
// CompatibleArch identifies the binary architecture of the archive.
// CompatibleSubArch is defined for specific values of Arch. Lastly
// CompatibleMachine is a free-form field which can be used to select
// machine-specific optimized builds.
//
// DownloadOffers contains the set of locations where the image may be retrieved
// from. Individual elements may be distinct CDN nodes or providers, for example
// in addition to a central location a locally running proxy may offer cached
// images which can be obtained at a lower cost.
type Archive struct {
	ArchiveName      string                  `json:"archive-name" yaml:"archive-name"`
	ArchiveSize      int64                   `json:"archive-size" yaml:"archive-size"`
	ArchiveType      ArchiveType             `json:"archive-type" yaml:"archive-type"`
	ArchiveFormat    ArchiveFormat           `json:"archive-format" yaml:"archive-format"`
	ArchiveChecksums map[ChecksumType]string `json:"archive-checksums,omitempty" yaml:"archive-checksums"`

	ContentType    ContentType    `json:"content-type" yaml:"content-type"`
	ContentFormat  ContentFormat  `json:"content-format" yaml:"content-format"`
	ContentVariant ContentVariant `json:"content-variant,omitempty" yaml:"content-variant,omitempty"`
	ContentSize    int64          `json:"content-size" yaml:"content-size"`

	CompatibleArch    Architecture `json:"compatible-arch" yaml:"compatible-arch"`
	CompatibleSubArch string       `json:"compatible-sub-arch,omitempty" yaml:"compatible-sub-arch,omitempty"`
	CompatibleMachine string       `json:"compatible-machine,omitempty" yaml:"compatible-machine,omitempty"`
	CompatibleDefault bool         `json:"compatible-default,omitempty" yaml:"compatible-default,omitempty"`

	DownloadOffers []DownloadOffer `json:"download-offers" yaml:"download-offers"`
}

// ArchiveType is the type of the container or envelope for used to deliver the content.
type ArchiveType string

const (
	// RaucBundle represents a RAUC bundle archive.
	RaucBundle ArchiveType = "rauc.bundle"
)

// ArchiveFormat is the format of the arachive.
// Archive format is specific to archive types.
type ArchiveFormat string

const (
	// RaucPlainBundle is an archive type for plain RAUC bundles.
	RaucPlainBundle ArchiveFormat = "rauc.bundle.plain"
	// RaucVerityBundle is an archive type for dm-verity RAUC bundles.
	RaucVerityBundle ArchiveFormat = "rauc.bundle.verity"
)

// ContentType is the type of content inside the package.
type ContentType string

const (
	// SysOTASystemImage denotes that archive content is a SysOTA system image.
	SysOTASystemImage ContentType = "sysota.system-image"
	// Source denotes that archive contains a collection of source code.
	Source ContentType = "source"
)

// ContentFormat is the format of the content inside the archive.
type ContentFormat string

const (
	// SquashfsV4CompXz is a squashfs v4 with xz compression.
	SquashfsV4CompXz ContentFormat = "squashfs.v4.xz"
	// TarGzArchive is a tar archive compressed with gzip.
	TarGzArchive ContentFormat = "tar.gz"
)

// ContentVariant is the variant of the content inside the package.
type ContentVariant string

const (
	// ReleaseVariant represents a typical release image.
	ReleaseVariant ContentVariant = "release"
	// TestVariant represents an image with additional test applications
	// and behavior.
	TestVariant ContentVariant = "test"
)

// ChecksumType is the algorithm used to compute the checksum of the package.
type ChecksumType string

const (
	// Sha256 is the sha-256 checksum algorithm.
	Sha256 ChecksumType = "sha256"
)

// DownloadOffer represents a single download URL.
//
// Metric helps to differentiate multiple offers. Lowest metric is the most
// desirable.
type DownloadOffer struct {
	URL    string `json:"url" yaml:"url"`
	Metric int    `json:"metric,omitempty" yaml:"metric,omitempty"`
}

// Architecture denotes CPU architecture.
type Architecture string

const (
	// NoArch identifies architecture independent packages.
	NoArch Architecture = "noarch"
	// X86 is the Intel/AMD 32bit CPU architecture.
	X86 Architecture = "x86"
	// X86_64 is the Intel/AMD 64bit CPU architecture.
	X86_64 Architecture = "x86-64"
	// Arm is the family of 32bit ARM architectures. The subarchitecture is
	// essential to make sense of this specific architecture.
	Arm Architecture = "arm"
	// Aarch64 is the family of 64bit ARM architectures.
	Aarch64 Architecture = "aarch64"
	// RiscV32 is the 32bit RISC-V CPU architecture.
	RiscV32 Architecture = "riscv32"
	// RiscV64 is the 64bit RISC-V CPU architecture.
	RiscV64 Architecture = "riscv64"
)
